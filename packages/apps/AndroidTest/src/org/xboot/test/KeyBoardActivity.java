package org.xboot.test;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;
import android.widget.TextView;
import android.util.Log;
import java.lang.ref.WeakReference;

public class KeyBoardActivity extends Activity {
    public static final String TAG = KeyBoardActivity.class.getSimpleName();
    private TextView mTextView;

    public static MyHandler mHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_keyboard);

        mTextView = (TextView) findViewById(R.id.keyValue);
        mTextView.setText("Please press any key!");
        mHandler = new MyHandler(this);
    }

    public static Handler getHandler() {
        return mHandler;
    }

    private static class MyHandler extends Handler {
        private WeakReference<KeyBoardActivity> mWeakReference;

        private MyHandler(KeyBoardActivity activity) {
            mWeakReference = new WeakReference<>(activity);
        }

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            Log.d(TAG, "handleMessage");
            KeyBoardActivity activity = mWeakReference.get();
            if (activity != null) {
                activity.handleMsg(msg);
            }

        }
    }

    private void handleMsg(Message msg) {
        Log.d(TAG, "handleMsg");
        switch (msg.what) {
            case 1:
                Bundle data = msg.getData();
                String text = new String();
                int code = data.getInt("code");
                Log.d(TAG, "keycode=" + code);
                switch (code) {
                    case KeyEvent.KEYCODE_MENU:
                        text += "MENU";
                        break;
                    case KeyEvent.KEYCODE_BACK:
                        text += "BACK";
                        break;
                    case KeyEvent.KEYCODE_VOLUME_UP:
                        text += "VOLUME_UP";
                        break;
                    case KeyEvent.KEYCODE_VOLUME_DOWN:
                        text += "VOLUME_DOWN";
                        break;
                    default:
                        text += "[" + code + "]";
                        break;
                }
                int action = data.getInt("action");
                switch (action) {
                    case KeyEvent.ACTION_DOWN:
                        text += " : [KEY DOWN]";
                        break;
                    case KeyEvent.ACTION_UP:
                        text += " : [KEY UP]";
                        break;
                    default:
                        break;
                }
                setText(text);
                break;
        }

    }

    private void setText(String text) {
        // if (isResumed()) {
            mTextView.setText(text);
        // }
    }

}
