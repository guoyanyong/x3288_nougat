package org.xboot.test;

import java.util.ArrayList;
import android.app.Activity;
import android.app.ActivityGroup;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import java.lang.reflect.Field;
import org.xboot.test.R;
import android.util.Log;

@SuppressWarnings("deprecation")
public class MainActivity extends ActivityGroup {
	public static final String TAG = MainActivity.class.getSimpleName();
	private View lcdView;
	private View tsView;
	private View ledView;
	private View beepView;
	private View fanView;
	private View backlightView;
	private View keyBoardView;
	private View batteryView;
	private View adcView;
	private View gsensorView;
	private View gyroscopeView;
	private View magneticView;
	private View lightView;
	private View audioView;
	private View cameraView;
	private View wifiView;
	private View networkView;
	private View serialView;
	private View sdcardView;
	private View udiskView;
	private ArrayList<View> views;
	private ViewPager mViewPager;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.main);
		views = new ArrayList<View>();
		mViewPager = (ViewPager) findViewById(R.id.main_viewpager);
		mViewPager.setOnPageChangeListener(null);
		mViewPager.setActivated(true);
		initView();
		addViews();

		PagerAdapter mPagerAdapter = new PagerAdapter() {
			@Override
			public boolean isViewFromObject(View arg0, Object arg1) {
				return arg0 == arg1;
			}

			@Override
			public int getCount() {
				return views.size();
			}

			@Override
			public Object instantiateItem(View container, int position) {
				View view = views.get(position);
				if(view == cameraView)
				{
					CameraActivity CActivity = (CameraActivity)getActivity(cameraView);
					CActivity.initial();
				}
				else if(view == batteryView)
				{
					BatteryActivity BActivity = (BatteryActivity)getActivity(batteryView);
					BActivity.initial();
				}
				else if(view == adcView)
				{
					AdcActivity AActivity = (AdcActivity) getActivity(adcView);
					AActivity.initial();
				}
				else if(view == gsensorView)
				{
					GSensorActivity GActivity = (GSensorActivity) getActivity(gsensorView);
					GActivity.initial();
				}
				else if(view == gyroscopeView)
				{
					GyroscopeActivity GyActivity = (GyroscopeActivity) getActivity(gyroscopeView);
					GyActivity.initial();
				}
				else if(view == magneticView)
				{
					MagneticActivity MaActivity = (MagneticActivity) getActivity(magneticView);
					MaActivity.initial();
				}
				else if(view == lightView)
				{
					LightActivity LiActivity = (LightActivity) getActivity(lightView);
					LiActivity.initial();
				}
				((ViewPager) container).addView(view);
				return view;
			}
			
			@Override
			public void destroyItem(View container, int position, Object object) {
				View view = views.get(position);
				if(view == cameraView)
				{
					CameraActivity CActivity = (CameraActivity)getActivity(cameraView);
					CActivity.release();
				}
				else if(view == batteryView)
				{
					BatteryActivity BActivity = (BatteryActivity) getActivity(batteryView);
					BActivity.release();
				}
				else if(view == adcView)
				{
					AdcActivity AActivity = (AdcActivity) getActivity(adcView);
					AActivity.release();
				}
				else if(view == gsensorView)
				{
					GSensorActivity GActivity = (GSensorActivity) getActivity(gsensorView);
					GActivity.release();
				}
				else if(view == gyroscopeView)
				{
					GyroscopeActivity GyActivity = (GyroscopeActivity) getActivity(gyroscopeView);
					GyActivity.release();
				}
				else if(view == magneticView)
				{
					MagneticActivity MaActivity = (MagneticActivity) getActivity(magneticView);
					MaActivity.release();
				}
				else if(view == lightView)
				{
					LightActivity LiActivity = (LightActivity) getActivity(lightView);
					LiActivity.release();
				}
				((ViewPager) container).removeView(view);
			}
		};
		mViewPager.setAdapter(mPagerAdapter);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();

		CameraActivity CActivity = (CameraActivity)getActivity(cameraView);
		CActivity.release();
		
		BatteryActivity BActivity = (BatteryActivity)getActivity(batteryView);
		BActivity.release();
		
		AdcActivity AActivity = (AdcActivity) getActivity(adcView);
		AActivity.release();
		
		GSensorActivity GActivity = (GSensorActivity) getActivity(gsensorView);
		GActivity.release();
		
		GyroscopeActivity GyActivity = (GyroscopeActivity) getActivity(gyroscopeView);
		GyActivity.release();
		
		MagneticActivity MaActivity = (MagneticActivity) getActivity(magneticView);
		MaActivity.release();
		
		LightActivity LiActivity = (LightActivity) getActivity(lightView);
		LiActivity.release();
	}

	public void initView() {
		lcdView = getViews(LCDActivity.class, "one");
		tsView = getViews(TSActivity.class, "one");
//		ledView = getViews(LedActivity.class, "one");
		beepView = getViews(BeepActivity.class, "one");
//		fanView = getViews(FanActivity.class, "one");
		backlightView = getViews(BacklightActivity.class, "one");
		keyBoardView = getViews(KeyBoardActivity.class, "one");
		batteryView = getViews(BatteryActivity.class, "one");
		adcView = getViews(AdcActivity.class, "one");
		gsensorView = getViews(GSensorActivity.class, "one");
		gyroscopeView = getViews(GyroscopeActivity.class, "one");
		magneticView = getViews(MagneticActivity.class, "one");
		lightView = getViews(LightActivity.class, "one");
		audioView = getViews(AudioActivity.class, "one");
		cameraView = getViews(CameraActivity.class, "one");
		wifiView=getViews(WifiActivity.class,"one");
		networkView = getViews(NetworkActivity.class, "one");
		serialView = getViews(SerialActivity.class, "one");
//		sdcardView = getViews(SdcardActivity.class, "one");
//		udiskView = getViews(UdiskActivity.class, "one");
	}

	public void addViews() {
		views.add(lcdView);
		views.add(tsView);
		//views.add(ledView);
		views.add(beepView);
		//views.add(fanView);
		views.add(backlightView);
		views.add(keyBoardView);
		views.add(batteryView);
		views.add(adcView);
		views.add(gsensorView);
		views.add(gyroscopeView);
		views.add(magneticView);
		views.add(lightView);
		views.add(audioView);
		views.add(cameraView);
		views.add(wifiView);
		views.add(networkView);
		views.add(serialView);
//		views.add(sdcardView);
//		views.add(udiskView);
	}

	public View getViews(Class<?> cls, String pageid) {
		return getLocalActivityManager().startActivity(
				pageid,
				new Intent(MainActivity.this, cls)
						.addFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT))
				.getDecorView();
	}

	public static Activity getActivity(View view) {
		Activity activity = null;
		if (view.getContext().getClass().getName().contains(
				"com.android.internal.policy.DecorContext"))
		{
			try
			{
				Field field = view.getContext().getClass().getDeclaredField(
						"mPhoneWindow");
				field.setAccessible(true);
				Object obj = field.get(view.getContext());
				java.lang.reflect.Method
				m1 = obj.getClass().getMethod("getContext");
				activity = (Activity)(m1.invoke(obj));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		else
		{
			activity = (Activity) view.getContext();
		}
		return activity;
	}
    
	@Override
	public boolean dispatchKeyEvent(KeyEvent event) {
		// if (keyBoardView == views.get(mViewPager.getCurrentItem())) {
		// 	if (event.getAction() == KeyEvent.ACTION_DOWN
		// 			|| event.getAction() == KeyEvent.ACTION_UP) {
		// 		Context context = keyBoardView.getContext();
		// 		if (context instanceof Activity) {
		// 			KeyBoardActivity activity = (KeyBoardActivity) context;
		// 			// Message message = new Message();
		// 			// message.what = 1;
		// 			// Bundle data = new Bundle();
		// 			// data.putInt("action", event.getAction());
		// 			// data.putInt("code", event.getKeyCode());
		// 			// message.setData(data);
		// 			// activity.mHandler.sendMessage(message);
		// 		}
		// 		return true;
		// 	}
		// }

        Log.d(TAG, "dispatchKeyEvent,keyCode" + event.getKeyCode() + ",action=" + event.getAction() + ",repeatCount=" + event.getRepeatCount());
        if (keyBoardView == views.get(mViewPager.getCurrentItem())) {
	        if (event.getAction() == KeyEvent.ACTION_DOWN || event.getAction() == KeyEvent.ACTION_UP) {
	            Message message = new Message();
	            message.what = 1;
	            Bundle data = new Bundle();
	            data.putInt("action", event.getAction());
	            data.putInt("code", event.getKeyCode());
	            message.setData(data);
	            if (KeyBoardActivity.getHandler() != null) {
	                KeyBoardActivity.getHandler().sendMessage(message);
	                return true;
	            }
	        }

    	}

		return super.dispatchKeyEvent(event);
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_MENU) {
			this.getLocalActivityManager().getCurrentActivity()
					.openOptionsMenu();
		} else if (keyCode == KeyEvent.KEYCODE_BACK) {
			finish();
		}
		return super.onKeyDown(keyCode, event);
	}
}
