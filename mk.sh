#!/bin/bash
#
# Description   : Android Build Script.
# Authors       : www.9tripod.com
# Version       : 1.00
# Notes         : None
#

#
# JAVA PATH
#
export PATH=/usr/lib/jvm/java-8-openjdk-amd64/bin:$PATH

#
# Some Directories
#
BS_DIR_TOP=$(cd `dirname $0` ; pwd)
BS_DIR_TOOLS=${BS_DIR_TOP}/tools
BS_DIR_RELEASE=${BS_DIR_TOP}/out/release
BS_DIR_TARGET=${BS_DIR_TOP}/out/target/product/rk3288
BS_DIR_UBOOT=${BS_DIR_TOP}/u-boot
BS_DIR_KERNEL=${BS_DIR_TOP}/kernel

#
# Target Config
#
BS_CONFIG_BOOTLOADER_UBOOT=x3288_defconfig
BS_CONFIG_KERNEL=x3288_defconfig
BS_CONFIG_KERNEL_DTB=x3288-cv5-bv4.img
#BS_CONFIG_KERNEL_DTB=x3288-cv4.-bv4.img
BS_CONFIG_FILESYSTEM=PRODUCT-rk3288-userdebug

setup_environment()
{
	LANG=C
	cd ${BS_DIR_TOP};
	PATH=${BS_DIR_TOP}/out/host/linux-x86/bin:$PATH;
	mkdir -p ${BS_DIR_RELEASE} || return 1
	[ -f "${BS_DIR_RELEASE}/upgrade_tool" ] || { cp -v ${BS_DIR_TOP}/RKTools/linux/Linux_Upgrade_Tool_1.27/upgrade_tool ${BS_DIR_RELEASE};}
	[ -f "${BS_DIR_RELEASE}/config.ini" ] || { cp -v ${BS_DIR_TOP}/RKTools/linux/Linux_Upgrade_Tool_1.27/config.ini ${BS_DIR_RELEASE};}
}

build_bootloader_uboot()
{
	# Compiler uboot
	cd ${BS_DIR_UBOOT} || return 1
	make distclean || return 1
	make ${BS_CONFIG_BOOTLOADER_UBOOT} || return 1
	make -j${threads} || return 1

	# Copy bootloader to release directory
	cp -av ${BS_DIR_UBOOT}/trust.img ${BS_DIR_RELEASE} || return 1
	cp -av ${BS_DIR_UBOOT}/rk3288_loader_*.bin ${BS_DIR_RELEASE}/MiniLoaderAll.bin || return 1
	cp -av ${BS_DIR_UBOOT}/uboot.img ${BS_DIR_RELEASE} || return 1

	return 0
}

build_kernel()
{
	# Compiler kernel
	cd ${BS_DIR_KERNEL} || return 1
	make ARCH=arm ${BS_CONFIG_KERNEL} || return 1
	#make ARCH=arm -j${threads} Image || return 1
	make ARCH=arm -j${threads} ${BS_CONFIG_KERNEL_DTB} || return 1

	# Copy kernel to release directory
	cp -v ${BS_DIR_KERNEL}/resource.img ${BS_DIR_RELEASE}
	cp -v ${BS_DIR_KERNEL}/kernel.img ${BS_DIR_RELEASE}

	find ${BS_DIR_TOP}/kernel/drivers/net/wireless/rockchip_wlan/*  -name "*.ko" | \
	    xargs -n1 -i cp {} ${BS_DIR_TOP}/vendor/rockchip/common/wifi/modules 

	#${BS_DIR_KERNEL}/scripts/dtc/dtc -I dtb -O dts -o ${BS_DIR_RELEASE}/${BS_CONFIG_KERNEL_DTB%%.img}.dts ${BS_DIR_KERNEL}/arch/arm/boot/dts/${BS_CONFIG_KERNEL_DTB%%.img}.dtb

	return 0
}

build_system()
{
	cd ${BS_DIR_TOP} || return 1
	source build/envsetup.sh || return 1
	#make installclean
	make -j${threads} ${BS_CONFIG_FILESYSTEM} || return 1

	echo -n "create boot.img without kernel... "
	[ -d ${BS_DIR_TARGET}/root ] && \
	mkbootfs ${BS_DIR_TARGET}/root | minigzip > ${BS_DIR_TARGET}/ramdisk.img && \
	        truncate -s "%4" ${BS_DIR_TARGET}/ramdisk.img && \
	${BS_DIR_TOP}/rkst/mkkrnlimg ${BS_DIR_TARGET}/ramdisk.img ${BS_DIR_RELEASE}/boot.img >/dev/null


PLATFORM_VERSION=`get_build_var PLATFORM_VERSION`
PLATFORM_SECURITY_PATCH=`get_build_var PLATFORM_SECURITY_PATCH`
TARGET_BUILD_VARIANT=`get_build_var TARGET_BUILD_VARIANT`
	echo -n "create recovery.img without kernel and resource... "
	[ -d ${BS_DIR_TARGET}/recovery/root ] && \
	mkbootfs ${BS_DIR_TARGET}/recovery/root | minigzip > ${BS_DIR_TARGET}/ramdisk-recovery.img && \
	        truncate -s "%4" ${BS_DIR_TARGET}/ramdisk-recovery.img && \
	#mkbootimg --kernel ${BS_DIR_TARGET}/kernel --ramdisk ${BS_DIR_TARGET}/ramdisk-recovery.img --os_version $PLATFORM_VERSION --os_patch_level $PLATFORM_SECURITY_PATCH --cmdline buildvariant=$TARGET_BUILD_VARIANT --output ${BS_DIR_TARGET}/recovery.img && \
	${BS_DIR_TOP}/rkst/mkkrnlimg ${BS_DIR_TARGET}/ramdisk-recovery.img ${BS_DIR_TARGET}/recovery.img
	cp -a ${BS_DIR_TARGET}/recovery.img ${BS_DIR_RELEASE}

	echo -n "create misc.img.... "
	cp -a ${BS_DIR_TOP}/rkst/Image/misc.img ${BS_DIR_RELEASE}/misc.img || return 1
	cp -a ${BS_DIR_TOP}/rkst/Image/pcba_small_misc.img ${BS_DIR_RELEASE}/pcba_small_misc.img || return 1
	cp -a ${BS_DIR_TOP}/rkst/Image/pcba_whole_misc.img ${BS_DIR_RELEASE}/pcba_whole_misc.img || return 1


BOARD_SYSTEMIMAGE_PARTITION_SIZE=`get_build_var BOARD_SYSTEMIMAGE_PARTITION_SIZE`
BOARD_USE_SPARSE_SYSTEM_IMAGE=`get_build_var BOARD_USE_SPARSE_SYSTEM_IMAGE`

if [ -d ${BS_DIR_TARGET}/system ]; then
	echo -n "create system.img..."
	if [ "$BOARD_USE_SPARSE_SYSTEM_IMAGE" = "true" ]; then
		if [ "${otapacket}" = no ]; then
			python ./build/tools/releasetools/build_image.py \
			${BS_DIR_TARGET}/system ${BS_DIR_TARGET}/obj/PACKAGING/systemimage_intermediates/system_image_info.txt \
			${BS_DIR_TARGET}/system.img ${BS_DIR_TARGET}/system
		fi
		python device/rockchip/common/sparse_tool.py ${BS_DIR_TARGET}/system.img
		mv ${BS_DIR_TARGET}/system.img.out ${BS_DIR_TARGET}/system.img
		cp -f ${BS_DIR_TARGET}/system.img ${BS_DIR_RELEASE}/system.img
	else
		#system_size=`ls -l ${BS_DIR_TARGET}/system.img | awk '{print $5;}'`
		system_size=$BOARD_SYSTEMIMAGE_PARTITION_SIZE
		[ $system_size -gt "0" ] || { echo "Please build android first!!!" && exit 1; }
		MAKE_EXT4FS_ARGS=" -L system -S ${BS_DIR_TARGET}/root/file_contexts -a system ${BS_DIR_RELEASE}/system.img ${BS_DIR_TARGET}/system"
		ok=0
		while [ "$ok" = "0" ]; do
			make_ext4fs -l $system_size $MAKE_EXT4FS_ARGS >/dev/null 2>&1 &&
			tune2fs -c -1 -i 0 $IMAGE_PATH/system.img >/dev/null 2>&1 &&
			ok=1 || system_size=$(($system_size + 5242880))
		done
		e2fsck -fyD ${BS_DIR_RELEASE}/system.img >/dev/null 2>&1 || true
	fi
	echo "done."
fi

	echo -n "create vendor.img..."
        cp -a ${BS_DIR_TARGET}/vendor0.img ${BS_DIR_RELEASE}/vendor0.img || return 1
        cp -a ${BS_DIR_TARGET}/vendor1.img ${BS_DIR_RELEASE}/vendor1.img || return 1
        echo "done."

	return 0
}


build_update()
{
	cd ${BS_DIR_RELEASE} || return 1
	#mkupdate.sh
	# Make update-android.img
	echo "create update-android.img..."
	cp -av ${BS_DIR_TOP}/device/rockchip/rk3288/parameter.txt ${BS_DIR_RELEASE} || return 1;
	cp -av ${BS_DIR_TOOLS}/package-file ${BS_DIR_RELEASE}/package-file || return 1;

	${BS_DIR_TOP}/RKTools/linux/Linux_Pack_Firmware/rockdev/afptool -pack ${BS_DIR_RELEASE}/ ${BS_DIR_RELEASE}/temp.img || return 1;
	${BS_DIR_TOP}/RKTools/linux/Linux_Pack_Firmware/rockdev/rkImageMaker -RK32 ${BS_DIR_RELEASE}/MiniLoaderAll.bin ${BS_DIR_RELEASE}/temp.img ${BS_DIR_RELEASE}/update-android.img -os_type:androidos || return 1;
	rm -fr ${BS_DIR_RELEASE}/temp.img || return 1;

	return 0
}

copy_other_files()
{
	cd ${BS_DIR_TOP} || return 1
	cp -av ${BS_DIR_TOP}/device/rockchip/rk3288/parameter.txt ${BS_DIR_RELEASE} || return 1;
	cp -av rkst/Image/misc.img ${BS_DIR_RELEASE}/misc.img || return 1;
	cp -av rkst/Image/pcba_small_misc.img ${BS_DIR_RELEASE}/pcba_small_misc.img || return 1;
	cp -av rkst/Image/pcba_whole_misc.img ${BS_DIR_RELEASE}/pcba_whole_misc.img || return 1;
	return 0
}

threads=1
uboot=no
kernel=no
system=no
update=no
otapacket=no

if [ -z $1 ]; then
	uboot=yes
	kernel=yes
	system=yes
	update=yes
fi

while [ "$1" ]; do
    case "$1" in
	-j=*)
		x=$1
		threads=${x#-j=}
	    ;;
	-u|--uboot)
		uboot=yes
	    ;;
	-k|--kernel)
		kernel=yes
	    ;;
	-s|--system)
		system=yes
	    ;;
	-U|--update)
		update=yes
	    ;;
	-cv=*)
		x=$1
		BOARD=${x#-cv=}
		echo "BOARD is ${BOARD}"
		if [ ${BOARD} = "5" ];then
			echo "change BS_CONFIG_KERNEL_DTB to x3288-cv5-bv4.img"
			BS_CONFIG_KERNEL_DTB=x3288-cv5-bv4.img
		fi
		if [ ${BOARD} = "4.1" ];then
			echo "change BS_CONFIG_KERNEL_DTB to x3288-cv4.1-bv4.img"
			BS_CONFIG_KERNEL_DTB=x3288-cv4.1-bv4.img
		fi
	    ;;
	-a|--all)
		uboot=yes
		kernel=yes
		system=yes
		update=yes
	    ;;
	-h|--help)
	    cat >&2 <<EOF
Usage: mk.sh [OPTION]
Build script for compile the source of telechips project.

  -j=n                 using n threads when building source project (example: -j=16)
  -u, --uboot          build bootloader uboot from source
  -k, --kernel         build kernel from source
  -s, --system         build android file system from source
  -U, --update         build update file
  -cv                  select CoreBoard Version(example: cv=4.1, cv=5)
  -a, --all            build all, include anything
  -h, --help           display this help and exit
EOF
	    exit 0
	    ;;
	*)
	    echo "mk.sh: Unrecognised option $1" >&2
	    exit 1
	    ;;
    esac
    shift
done

setup_environment || exit 1
#copy_other_files || exit 1

if [ "${uboot}" = yes ]; then
	build_bootloader_uboot || exit 1
fi

if [ "${kernel}" = yes ]; then
	build_kernel || exit 1
fi

if [ "${system}" = yes ]; then
	build_system || exit 1
fi

if [ "${update}" = yes ]; then
	build_update || exit 1
fi

exit 0
