/dts-v1/;
#include <dt-bindings/sensor-dev.h>
#include <dt-bindings/pinctrl/rockchip.h>
#include "rk3288-9tripod.dtsi"
#include "rk3288-android.dtsi"

#include "lcd-rgb1024x600-VS070CXN.dtsi" //7 inch rgb 1024x600
//#include "lcd-mipi1024x600-WY070ML.dtsi" //7 inch mipi 1024x600
//#include "lcd-lvds1280x800-EJ101IA.dtsi" //10.1 inch lvds 1280x800


/ {
	compatible = "rockchip,x3288-cv5", "rockchip,rk3288";

	backlight: backlight {
		compatible = "pwm-backlight";
		brightness-levels = <
			  0   1   2   3   4   5   6   7
			  8   9  10  11  12  13  14  15
			 16  17  18  19  20  21  22  23
			 24  25  26  27  28  29  30  31
			 32  33  34  35  36  37  38  39
			 40  41  42  43  44  45  46  47
			 48  49  50  51  52  53  54  55
			 56  57  58  59  60  61  62  63
			 64  65  66  67  68  69  70  71
			 72  73  74  75  76  77  78  79
			 80  81  82  83  84  85  86  87
			 88  89  90  91  92  93  94  95
			 96  97  98  99 100 101 102 103
			104 105 106 107 108 109 110 111
			112 113 114 115 116 117 118 119
			120 121 122 123 124 125 126 127
			128 129 130 131 132 133 134 135
			136 137 138 139 140 141 142 143
			144 145 146 147 148 149 150 151
			152 153 154 155 156 157 158 159
			160 161 162 163 164 165 166 167
			168 169 170 171 172 173 174 175
			176 177 178 179 180 181 182 183
			184 185 186 187 188 189 190 191
			192 193 194 195 196 197 198 199
			200 201 202 203 204 205 206 207
			208 209 210 211 212 213 214 215
			216 217 218 219 220 221 222 223
			224 225 226 227 228 229 230 231
			232 233 234 235 236 237 238 239
			240 241 242 243 244 245 246 247
			248 249 250 251 252 253 254 255>;
		default-brightness-level = <128>;
		enable-gpios = <&gpio7 2 GPIO_ACTIVE_HIGH>;
		pinctrl-names = "default";
		pinctrl-0 = <&bl_en>;
		pwms = <&pwm1 0 1000000 0>;
	};

	sdio_pwrseq: sdio-pwrseq {
		compatible = "mmc-pwrseq-simple";
		clocks = <&hym8563>;
		clock-names = "ext_clock";
		pinctrl-names = "default";
		pinctrl-0 = <&wifi_enable_h>;

		/*
		 * On the module itself this is one of these (depending
		 * on the actual card populated):
		 * - SDIO_RESET_L_WL_REG_ON
		 * - PDN (power down when low)
		 */
		reset-gpios = <&gpio4 28 GPIO_ACTIVE_LOW>;
	};

	vcc_lcd: vcc-lcd {
		compatible = "regulator-fixed";
		enable-active-high;
		gpio = <&gpio7 3 GPIO_ACTIVE_HIGH>;
		pinctrl-names = "default";
		pinctrl-0 = <&lcd_en>;
		regulator-name = "vcc_lcd";
		vin-supply = <&vcc_io>;
	};

	vcc_wl: vcc-wl {
		compatible = "regulator-fixed";
		enable-active-high;
		gpio = <&gpio7 9 GPIO_ACTIVE_HIGH>;
		pinctrl-names = "default";
		pinctrl-0 = <&wifi_pwr>;
		regulator-name = "vcc_wl";
		vin-supply = <&vcc_18>;
	};

	wireless-bluetooth {
		compatible = "bluetooth-platdata";
		uart_rts_gpios = <&gpio4 19 GPIO_ACTIVE_LOW>;
		pinctrl-names = "default", "rts_gpio";
		pinctrl-0 = <&uart0_rts>;
		pinctrl-1 = <&uart0_gpios>;
		BT,reset_gpio    = <&gpio4 29 GPIO_ACTIVE_HIGH>;
		BT,wake_gpio     = <&gpio4 26 GPIO_ACTIVE_HIGH>;
		BT,wake_host_irq = <&gpio4 31 GPIO_ACTIVE_HIGH>;
		status = "okay";
	};

	wireless-wlan {
		compatible = "wlan-platdata";
		rockchip,grf = <&grf>;
		wifi_chip_type = "ap6255";
		sdio_vref = <1800>;
		//WIFI,poweren_gpio = <&gpio4 RK_PD4 GPIO_ACTIVE_HIGH>;
		WIFI,host_wake_irq = <&gpio4 30 GPIO_ACTIVE_HIGH>;
		//WIFI,reset_gpio = <&gpio0 RK_PA2 GPIO_ACTIVE_LOW>;
		status = "okay";
	};

	xgpio_beep {
		status = "okay";
		compatible = "9tripod,beep";
		pinctrl-names = "default";
		pinctrl-0 = <&beep_gpio>;
		gpio = <&gpio6 RK_PB3 GPIO_ACTIVE_HIGH>;
	};
};

&cif_isp0 {
	rockchip,camera-modules-attached = <&camera0>;
	status = "disabled";
};

&cpu0 {
	cpu-supply = <&vdd_cpu>;
};

&cpu0_opp_table {
	clocks = <&cru PLL_APLL>;
	leakage-scaling-sel = <0   254   25>;

	opp-1800000000 {
		opp-hz = /bits/ 64 <1800000000>;
		opp-microvolt = <1350000>;
		clock-latency-ns = <40000>;
		status = "disabled";
	};
};

&dfi {
	status = "okay";
};

&dmc {
	center-supply = <&vdd_log>;
	status = "okay";
	vop-dclk-mode = <1>;
};

&gpu {
	status = "okay";
	mali-supply = <&vdd_gpu>;
};

&i2c0 {
	status = "okay";
	clock-frequency = <400000>;

	vdd_cpu: syr827@40 {
		compatible = "silergy,syr827";
		fcs,suspend-voltage-selector = <1>;
		reg = <0x40>;
		regulator-name = "vdd_cpu";
		regulator-min-microvolt = <850000>;
		regulator-max-microvolt = <1350000>;
		regulator-always-on;
		regulator-boot-on;
		regulator-enable-ramp-delay = <300>;
		regulator-ramp-delay = <8000>;
		vin-supply = <&vcc_sys>;
		regulator-state-mem {
			regulator-off-in-suspend;
		};
	};

	vdd_gpu: syr828@41 {
		compatible = "silergy,syr828";
		fcs,suspend-voltage-selector = <1>;
		reg = <0x41>;
		regulator-name = "vdd_gpu";
		regulator-min-microvolt = <850000>;
		regulator-max-microvolt = <1350000>;
		regulator-always-on;
		regulator-ramp-delay = <6000>;
		vin-supply = <&vcc_sys>;
		regulator-state-mem {
			regulator-off-in-suspend;
		};
	};

	hym8563: hym8563@51 {
		compatible = "haoyu,hym8563";
		reg = <0x51>;

		interrupt-parent = <&gpio0>;
		interrupts = <4 IRQ_TYPE_EDGE_FALLING>;

		pinctrl-names = "default";
		pinctrl-0 = <&pmic_int>;

		#clock-cells = <0>;
		clock-output-names = "xin32k";
	};

	act8846: act8846@5a {
		compatible = "active-semi,act8846";
		reg = <0x5a>;
		status = "okay";
		gpios =<&gpio7 RK_PB6 GPIO_ACTIVE_LOW>,<&gpio0 RK_PB2 GPIO_ACTIVE_HIGH>;
		vp1-supply = <&vcc_sys>;
		vp2-supply = <&vcc_sys>;
		vp3-supply = <&vcc_sys>;
		vp4-supply = <&vcc_sys>;
		inl1-supply = <&vcc_io>;
		inl2-supply = <&vcc_sys>;
		inl3-supply = <&vcc_20>;

		regulators {
			vcc_ddr: REG1 {
				regulator-name = "VCC_DDR";
				regulator-min-microvolt = <1200000>;
				regulator-max-microvolt = <1200000>;
				regulator-always-on;
			};

			vcc_io: REG2 {
				regulator-name = "VCC_IO";
				regulator-min-microvolt = <3300000>;
				regulator-max-microvolt = <3300000>;
				regulator-always-on;
			};

			vdd_log: REG3 {
				regulator-name = "VDD_LOG";
				regulator-min-microvolt = <1150000>;
				regulator-max-microvolt = <1150000>;
				regulator-always-on;
			};

			vcc_20: REG4 {
				regulator-name = "VCC_20";
				regulator-min-microvolt = <2000000>;
				regulator-max-microvolt = <2000000>;
				regulator-always-on;
			};

			vccio_sd: REG5 {
				regulator-name = "VCCIO_SD";
				regulator-min-microvolt = <1800000>;
				regulator-max-microvolt = <3300000>;
				regulator-always-on;
			};

			vdd10_lcd: REG6 {
				regulator-name = "VDD10_LCD";
				regulator-min-microvolt = <1000000>;
				regulator-max-microvolt = <1000000>;
				regulator-always-on;
			};

			vcca_codec: REG7 {
				regulator-name = "VCCA_CODEC";
				regulator-min-microvolt = <3300000>;
				regulator-max-microvolt = <3300000>;
				regulator-always-on;
			};

			vcca_tp: REG8 {
				regulator-name = "VCCA_TP";
				regulator-min-microvolt = <3300000>;
				regulator-max-microvolt = <3300000>;
				regulator-always-on;
			};

			vccio_pmu: REG9 {
				regulator-name = "VCCIO_PMU";
				regulator-min-microvolt = <3300000>;
				regulator-max-microvolt = <3300000>;
				regulator-always-on;
			};

			vdd_10: REG10 {
				regulator-name = "VDD_10";
				regulator-min-microvolt = <1000000>;
				regulator-max-microvolt = <1000000>;
				regulator-always-on;
			};

			vcc_18: REG11 {
				regulator-name = "VCC_18";
				regulator-min-microvolt = <1800000>;
				regulator-max-microvolt = <1800000>;
				regulator-always-on;
			};

			vcc18_lcd: REG12 {
				regulator-name = "VCC18_LCD";
				regulator-min-microvolt = <1800000>;
				regulator-max-microvolt = <1800000>;
				regulator-always-on;
			};
		};
	};
};

&i2c1 {
	status = "okay";
	clock-frequency = <400000>;

	sensor@1d {
		status = "okay";
		compatible = "gs_mma8452";
		reg = <0x1d>;
		type = <SENSOR_TYPE_ACCEL>;
		irq-gpio = <&gpio8 RK_PA0 IRQ_TYPE_EDGE_FALLING>;
		irq_enable = <1>;
		poll_delay_ms = <30>;
		layout = <3>;
	};

	mpu6050@68 {
		status = "disabled";
		compatible = "invensense,mpu6050";
		pinctrl-names = "default";
		pinctrl-0 = <&mpu6050_irq_gpio>;
		reg = <0x68>;
		irq-gpio = <&gpio8 0 IRQ_TYPE_EDGE_RISING>;
		mpu-int_config = <0x10>;
		mpu-level_shifter = <0>;
		mpu-orientation = <0 1 0 1 0 0 0 0 1>;
		orientation-x= <0>;
		orientation-y= <1>;
		orientation-z= <0>;
		support-hw-poweroff = <1>;
		mpu-debug = <1>;
	};
};

&i2c2 {
	status = "okay";

	es8323: es8323@10 {
		status = "okay";
		#sound-dai-cells = <0>;
		compatible = "everest,es8323";
		reg = <0x10>;
		//spk-con-gpio = <&gpio0 10 GPIO_ACTIVE_HIGH>;   //GPIO_B2
		//hp-det-gpio = <&gpio7 15 GPIO_ACTIVE_LOW>;      //GPIO_B7
		clocks = <&cru SCLK_I2S0_OUT>;
		clock-names = "mclk";
		//interrupt-parent = <&gpio6>;
		//interrupts = <7 IRQ_TYPE_EDGE_FALLING>;
		pinctrl-names = "default";
		pinctrl-0 = <&i2s0_mclk>;
	};

	rt5640: rt5640@1c {
		status = "disabled";
		#sound-dai-cells = <0>;
		compatible = "realtek,rt5640";
		reg = <0x1c>;
		clocks = <&cru SCLK_I2S0_OUT>;
		clock-names = "mclk";
		interrupt-parent = <&gpio6>;
		interrupts = <7 IRQ_TYPE_EDGE_FALLING>;
		pinctrl-names = "default";
		pinctrl-0 = <&i2s0_mclk>;
	};
};

&i2c3 {
	status = "okay";

	camera0: camera-module@10 {
		status = "disabled";

		compatible = "omnivision,ov8858-v4l2-i2c-subdev";
		reg = <0x10>;
		device_type = "v4l2-i2c-subdev";

		clocks = <&cru SCLK_VIP_OUT>;
		clock-names = "clk_cif_out";

		pinctrl-names = "rockchip,camera_default",
			"rockchip,camera_sleep";
		pinctrl-0 = <&cam0_default_pins>;
		pinctrl-1 = <&cam0_sleep_pins>;

		rockchip,pd-gpio = <&gpio2 15 GPIO_ACTIVE_LOW>;
		rockchip,pwr-gpio = <&gpio0 17 GPIO_ACTIVE_HIGH>;

		rockchip,camera-module-mclk-name = "clk_cif_out";
		rockchip,camera-module-dovdd = "1.8v";
		rockchip,camera-module-facing = "back";
		rockchip,camera-module-name = "cmk-cb0695-fv1";
		rockchip,camera-module-len-name = "lg9569a2";
		rockchip,camera-module-fov-h = "66.0";
		rockchip,camera-module-fov-v = "50.1";
		rockchip,camera-module-orientation = <0>;
		rockchip,camera-module-iq-flip = <0>;
		rockchip,camera-module-iq-mirror = <0>;
		rockchip,camera-module-flip = <0>;
		rockchip,camera-module-mirror = <0>;

		/* resolution.w, resolution.h, defrect.left, defrect.top, defrect.w, defrect.h */
		rockchip,camera-module-defrect0 = <3264 2448 0 0 3264 2448>;
		rockchip,camera-module-flash-support = <0>;
		rockchip,camera-module-mipi-dphy-index = <0>;
	};
};

&i2c4 {
	status = "okay";
	clock-frequency = <400000>;

/*touchscreen devicetree node move to some lcd dtsi file
	gslx680@40 {
		status = "okay";
		compatible = "9tripod,gslx680";
		reg = <0x40>;
		touch-gpio = <&gpio7 RK_PA6 IRQ_TYPE_EDGE_RISING>;
		reset-gpio = <&gpio7 RK_PA4 GPIO_ACTIVE_LOW>;
		max-x = <1024>;
		max-y = <600>;
	};

	ts@14 {
		status = "okay";
		compatible = "goodix,gt9xx";
		reg = <0x14>;
		touch-gpio = <&gpio7 RK_PA6 IRQ_TYPE_EDGE_RISING>;
		reset-gpio = <&gpio7 RK_PA4 GPIO_ACTIVE_LOW>;
		max-x = <1280>;
		max-y = <800>;
	};

	gsl3673@40 {
		compatible = "GSL,GSL3673";
		reg = <0x40>;
		screen_max_x = <1536>;
		screen_max_y = <2048>;
		irq_gpio_number = <&gpio7 6 IRQ_TYPE_LEVEL_LOW>;
		rst_gpio_number = <&gpio7 5 GPIO_ACTIVE_HIGH>;
		status = "okay";
	};

	ts@01 {
		compatible = "ct,vtl_ts";
		reg = <0x01>;
		screen_max_x = <1536>;
		screen_max_y = <2048>;
		xy_swap = <1>;
		x_reverse = <0>;
		y_reverse = <0>;
		x_mul = <2>;
		y_mul = <2>;
		bin_ver = <0>;
		irq_gpio_number = <&gpio7 6 IRQ_TYPE_LEVEL_LOW>;
		rst_gpio_number = <&gpio7 5 GPIO_ACTIVE_HIGH>;
	};
*/
};

&sdio0 {
	status = "okay";

	clock-frequency = <50000000>;
	clock-freq-min-max = <200000 50000000>;

	bus-width = <4>;
	cap-sd-highspeed;
	//cap-sdio-irq;
	disable-wp;
	keep-power-in-suspend;
	mmc-pwrseq = <&sdio_pwrseq>;
	non-removable;
	num-slots = <1>;
	pinctrl-names = "default";
	pinctrl-0 = <&sdio0_bus4 &sdio0_cmd &sdio0_clk>;//&sdio0_int
	sd-uhs-sdr104;
	supports-sdio;
};

&lvds_panel {
	power-supply = <&vcc_lcd>;
};

&rga {
	status = "okay";
};

&rockchip_suspend {
	status = "okay";
};

&sound {
	status = "okay";
};

&tsadc {
	status = "disabled";
};

&uart0 {
	status = "okay";
};

&uart2 {
	status = "okay";
};

/*
 * Due to not have the software of PWM for remotectrl.
 * We can _*HACK*_ do that as the following.
 */
&pwm0 {
	compatible = "rockchip,remotectl-pwm";
	remote_pwm_id = <0>;
	handle_cpu_id = <1>;
	status = "okay";

	ir_key1{
		rockchip,usercode = <0x1980>;
		rockchip,key_table =
			<0xef   KEY_POWER>,
			<0xe2   0xe2>,
			<0xe0   KEY_MUTE>,
			<0xe8   KEY_UP>,
			<0xe9   KEY_DOWN>,
			<0xb0   KEY_LEFT>,
			<0xae   KEY_RIGHT>,
			<0xaf   KEY_ENTER>,
			<0xe6   KEY_HOME>,
			<0xe7   KEY_MENU>,
			<0xe5   KEY_BACK>,
			<0xe3   KEY_VOLUMEUP>,
			<0xb9   KEY_VOLUMEDOWN>,
			<0xb6   0xb6>,
			<0xa5   0xa5>;
	};
};

&pwm1 {
	status = "okay";
};

&pinctrl {
	backlight {
		bl_en: bl-en {
			rockchip,pins = <7 RK_PA2 RK_FUNC_GPIO &pcfg_pull_none>;
		};
	};

	buttons {
		pwrbtn: pwrbtn {
			rockchip,pins = <0 5 RK_FUNC_GPIO &pcfg_pull_up>;
		};
	};

	xgpio_beep {
		beep_gpio: beep-gpio {
			rockchip,pins = <6 RK_PB3 RK_FUNC_GPIO &pcfg_pull_up>;
		};
	};

	cam_pins {
		cam0_default_pins: cam0-default-pins {
			rockchip,pins = <0 17 RK_FUNC_GPIO &pcfg_pull_none>,
					<2 15 RK_FUNC_GPIO &pcfg_pull_none>,
					<2 11 RK_FUNC_1 &pcfg_pull_none>;
		};
		cam0_sleep_pins: cam0-sleep-pins {
			rockchip,pins = <0 17 RK_FUNC_GPIO &pcfg_pull_none>,
					<2 15 RK_FUNC_GPIO &pcfg_pull_none>,
					<2 11 RK_FUNC_GPIO &pcfg_pull_none>;
		};
	};

	lcd {
		lcd_en: lcd-en  {
			rockchip,pins = <7 3 RK_FUNC_GPIO &pcfg_pull_none>;
		};
	};

	sdio-pwrseq {
		wifi_enable_h: wifi-enable-h {
			rockchip,pins = <4 28 RK_FUNC_GPIO &pcfg_pull_none>;
		};
	};

	wifi {
		wifi_pwr: wifi-pwr {
			rockchip,pins = <7 9 RK_FUNC_GPIO &pcfg_pull_none>;
		};
	};

	mpu6050 {
		mpu6050_irq_gpio: mpu6050-irq-gpio {
			rockchip,pins = <8 0 RK_FUNC_GPIO &pcfg_pull_none>;
		};
	};
};
